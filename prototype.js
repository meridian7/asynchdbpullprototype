

// firebase config 
var firebaseConfig = {
    apiKey: "AIzaSyCXCfDagK8KH0XEYc9mBp_o6g1kEeJ-OKY",
    authDomain: "master-dreamer-305905.firebaseapp.com",
    databaseURL: "https://master-dreamer-305905-default-rtdb.firebaseio.com",
    projectId: "master-dreamer-305905",
    storageBucket: "master-dreamer-305905.appspot.com",
    messagingSenderId: "572805183937",
    appId: "1:572805183937:web:08883bf0a6716b31dc1740"
};
firebase.initializeApp(firebaseConfig);
let referenceToOldestKey = '';
function loadInit() {
    document.getElementById("string").innerHTML = "";
    firebase.database().ref('strings')
        .orderByKey()
        .limitToLast(15)
        .once('value')
        .then((snapshot) => {
            let arrayOfKeys = Object.keys(snapshot.val())
                .sort()
                .reverse();

            let results = arrayOfKeys
                .map((key) => snapshot.val()[key]);
            referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1];
            for (var i = 0; i < results.length; i++) {
                var obj = results[i].string;

                document.getElementById("string").innerHTML += "<h1>" + obj + "</h1>";
            }
        })
        .catch((error) => { console.log(error) });
}

loadInit();


let BtnEle = document.querySelector("#Btn");
BtnEle.addEventListener("click", () => {
    writeUserData();
});

function writeUserData() {
    firebase.database().ref('strings').push({
        string: "string new"
    });
    loadInit();
}


let BtnElem = document.querySelector("#load");
BtnElem.addEventListener("click", () => {
    firebase.database().ref('strings')
        .orderByKey()
        .endAt(referenceToOldestKey)
        .limitToLast(15)
        .once('value')
        .then((snapshot) => {
            let arrayOfKeys = Object.keys(snapshot.val())
                .sort()
                .reverse()
                .slice(1);
            let results = arrayOfKeys
                .map((key) => snapshot.val()[key]);
            referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1];
            for (var i = 0; i < results.length; i++) {
                var obj = results[i].string;

                document.getElementById("string").innerHTML += "<h1>" + obj + "</h1>";
            }

        })
        .catch((error) => { console.log(error) });
}
);